#   autor Théo Berguig
#   lab : Lienss
import numpy as np
from netCDF4 import Dataset
import os
import warnings

newpath = './build/'
if not os.path.exists(newpath):
    os.makedirs(newpath)


# Ouverture du fichier pour écriture, on peut spécifier le format="NETCDF4_CLASSIC" ou "NETCDF3_CLASSIC"
# par défault on aura NETCDF4

ds = Dataset(newpath+"tuto.nc", mode="w")

# Nous allons créer un champs 2D: (20,20)

ds.createDimension("x", 20)
ds.createDimension("y", 20)
ds.createDimension("time", None)

# Nous allons créer deux variables : var1 et var2 et définir leurs dimensions.

var1 = ds.createVariable("field1", "f4", ("time", "x", "y"))
var2 = ds.createVariable("field2", "f4", ("time", "x", "y"))

#add netcdf attributes
var1.units = "Celcius"
var1.long_name = "Surface air temperature"

var2.units = "Kelvin"
var2.long_name = "Surface air temperature"

# add data

data = np.random.randn(10, 20, 20)
var1[:] = data
var2[:] =  data + 273.15
print(var1.shape)
ds.close();