#   autor Théo Berguig
#   lab : Lienss

import os

import numpy as np
import pandas as pad
import xarray as xar

import random

import time

# ========= CONFIG ========
newpath = './build/'
if not os.path.exists(newpath):
    os.makedirs(newpath)

# def gen_simulated_data(minn,maxx):
#    return rund(random.randint(minn,maxx) + random.random(),4)

array_date = np.arange('2005-02', '2005-03', dtype='datetime64[s]').astype('datetime64[ns]')

NB_DATA = len(array_date)

# ==========================
def gen_data():
# longitude : -1.2399120757067439
# latitude : 46.16048464781507
    D = {
    "datetime": array_date,
    "long": np.random.uniform(-1.23991207,-1.239912009,NB_DATA),
    "lati":np.random.uniform(46.160485,46.160489,NB_DATA),
    "methan": np.random.uniform(50,800,NB_DATA),
    "chlorophyle":np.random.uniform(3,35,NB_DATA),

    }
    return pad.DataFrame(D)

def save_in_NetCDF(dataF):
    newpath = './build/'
    if not os.path.exists(newpath):
        os.makedirs(newpath)

    # automated 
    coluns = dataF.columns

    for c in coluns : 
        if  c.startswith("tim") or c == "time" or c.startswith("dateti"):
            pass
        if c.startswith("lati") or c == "latitude" :
            pass
        if c.startswith("long") or c == "longitude" :
            pass
s
    ds = xar.Dataset(
        data_vars=dict(
            methan=(["time"], dataF["methan"]),
            chlorophyle=(["time"], dataF["chlorophyle"]),
        ),
        coords=dict(
            lon=(["y"], dataF["long"]),
            lat=(["x"], dataF["lati"]),
            time=dataF["datetime"],
            reference_time=dataF["datetime"][0],
        ),
        attrs=dict(description="randome testing data for dev savaing netcdf data"),
    )
    ds.to_netcdf(newpath+'tuto_3.nc')

if __name__ == '__main__':
    tic = time.time_ns()
    test = gen_data()
    toc = tic - time.time_ns()
    print("\t time gen data : ", abs(toc),"ns")
    print("---"*10)
    print(test.describe)
    
    tic = toc = 0

    tic = time.time_ns()
    save_in_NetCDF(test)
    toc = tic - time.time_ns()
    print("\t time gen data : ", abs(toc),"ns")
    