#   autor Théo Berguig
#   lab : Lienss

import os

import numpy as np
import pandas as pd
import xarray as xar

# creating randome data

np.random.seed(0)
temperature = 15 + 8 * np.random.randn(2, 2, 3)
precipitation = 10 * np.random.rand(2, 2, 3)
lon = [[-99.83, -99.32], [-99.79, -99.23]]
lat = [[42.25, 42.21], [42.63, 42.59]]
time = pd.date_range("2014-09-06", periods=3)
reference_time = pd.Timestamp("2014-09-05")

print(time)
print("-"*12)
print(reference_time)

# creat folder path if not existe

newpath = './build/'
if not os.path.exists(newpath):
    os.makedirs(newpath)

ds = xar.Dataset(
    data_vars=dict(
        temperature=(["x", "y", "time"], temperature),
        precipitation=(["x", "y", "time"], precipitation),
    ),
    coords=dict(
        lon=(["x", "y"], lon),
        lat=(["x", "y"], lat),
        time=time,
        reference_time=reference_time,
    ),
    attrs=dict(description="Weather related data."),
)
print(ds)
print("-"*12)
print(ds.time)
print("-"*12)
print(help(type(ds)))

# ds.to_netcdf(newpath+'tuto_2.nc')