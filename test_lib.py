#   autor Théo Berguig
#   lab : Lienss

import time 
import os
from netCDF4 import Dataset


newpath = './build/'
if not os.path.exists(newpath):
    os.makedirs(newpath)

rootgrp = Dataset(newpath+"test.nc", "w")

rootgrp.description = "bogus example script"
rootgrp.history = "Created " + time.ctime(time.time())
rootgrp.source = "netCDF4 python module tutorial"

fcstgrp = rootgrp.createGroup("forecasts")
analgrp = rootgrp.createGroup("analyses")

print(rootgrp.groups)

level = rootgrp.createDimension("level", None)

time = rootgrp.createDimension("time", None)

lat = rootgrp.createDimension("lat", 73)
lon = rootgrp.createDimension("lon", 144)

print(rootgrp.dimensions)

print(len(lon))

print(lon.isunlimited())

print(time.isunlimited())

print("---")
times = rootgrp.createVariable("time","f8",("time",))
levels = rootgrp.createVariable("level","i4",("level",))
latitudes = rootgrp.createVariable("lat","f4",("lat",))
longitudes = rootgrp.createVariable("lon","f4",("lon",))
# two dimensions unlimited
temp = rootgrp.createVariable("temp","f4",("time","level","lat","lon",))
temp.units = "K"

print(temp)
print("---")

ftemp = rootgrp.createVariable("/forecasts/model1/temp","f4",("time","level","lat","lon",))

print(rootgrp.variables)


for name in rootgrp.ncattrs():
    print("Global attr {} = {}".format(name, getattr(rootgrp, name)))
print("---")

import numpy as np
lats =  np.arange(-90,91,2.5)
lons =  np.arange(-180,180,2.5)
latitudes[:] = lats
longitudes[:] = lons
print("latitudes =\n{}".format(latitudes[:]))

print(rootgrp.data_model)
rootgrp.close()